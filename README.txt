#############################################
		Readme
#############################################

Cette archive est le rendu final d'un projet effectuer dans le cadre de 
l'UE MOGPL de M1 - ANDROIDE de Sorbonne Université le 3 janvier 2021 par
Musquer Basile et Lechapelier Marius. Elle contient:
- Le code source du projet (fichier .ipynb)
- Le rapport du projet (fichier .pdf)
- Un fichier de données (fichier .csv)
- Le sujet du projet (fichier .pdf)

**********************
	Présentation
**********************
Le code (.ipynb) est partagé en quatre parties:
- Une initialisation qui récupère les données du .csv
- Une partie pour chaque exercice

Chaque question de code de chaque exercice est répondu au sein d'une fonction.
Elles suivent strictement l'ordre du sujet.
Ces dernières comprennent vérifications des paramètres, résolutions du problème
et affichage, pour plus de détails consulté le code.

Paramètres par défaut:
- Alpha = 0.2
- Matrice de distance: issue de l'initialisation
- Noms des villes: issue de l'initialisation
- Pour 1_2: J est généré aléatoirement si non explicité par l'utilisateur

Seul k doit être explicitement renseigné par l'utilisateur.

En dessous de chaque fonction se trouve plusieurs tests avec leur résultats.


Note: Plusieurs boîte contenant du code sont non éxécutables.
Elles correspondent à des tests d'implémentation ou des vérifications de résultats.
**********************
	Utilisation
**********************
Pour utiliser notre code, nous recommandons:

Installer Miniconda 3 pour python 3.8: https://docs.conda.io/en/latest/miniconda.html

Installer Jupyter Notebook: https://jupyter.org/install

Insaller les packages suivants, si besoin, à partir du cloud d'anaconda:
- numpy
- csv
- math
- networkx
- (itertools)

Ouvrez l'invite de commande de miniconda, aller à l'emplacement du projet et
taper: jupyter-notebook

Dans le navigateur ainsi ouvert, executer les différentes boîtes avec Maj+Enter
pour dérouler l'execution des programmes.

Vous pouvez modifier les paramètres des fonctions dans les boîtes dédiées aux tests.
Ex: q2_1(k=6, alpha=0.05)

